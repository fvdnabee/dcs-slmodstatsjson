# SlmodStats to JSON script
This lua script loads the stats table from SlmodStats.lua and dumps it to
SlmodStats.json on disk.

Note the JSON library https://github.com/rxi/json.lua, does not support
pretty encoding.

# Usage
Place the SlmodStats.lua file in the same directory as the
`slmodstats_to_json.lua` script and run:
```
lua slmodstats_to_json.lua
```
If successful, the JSON file is available at SlmodStats.json.

# Thank you
* SlmodSats
* rxi's JSON lua library.
