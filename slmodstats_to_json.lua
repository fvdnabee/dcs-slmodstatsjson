--[[
   SlmodStats to JSON script

   This lua script loads the stats table from SlmodStats.lua and dumps it to
   SlmodStats.json on disk.

   Note the JSON library https://github.com/rxi/json.lua, does not support
   pretty encoding.
--]]

function tableLength(T)
	local count = 0
	for _ in pairs(T) do count = count + 1 end
	return count
end

function writeFile(contents, filename)
	file = io.open(filename, "w")
	io.output(file)
	io.write(contents)
	io.close(file)
end

function getPlayers(stats)
	-- order stats table by keys
	local ordered_keys = {}
	for k in pairs(stats) do
	    table.insert(ordered_keys, k)
	end
	table.sort(ordered_keys)

	local result = ""
	for i = 1, #ordered_keys do
		local k, v = ordered_keys[i], stats[ ordered_keys[i] ]
		result = result .. v['names'][1] .. ", "
		if i == #ordered_keys then
			result = result:sub(0, -3)  -- remove comma+space
		end
	end

	return result
end

local json = require("json")
if not json then
	print("Failed to load json.lua")
	os.exit()
end
if not require("SlmodStats") then
	print("Failed to load SlmodStats.lua")
	os.exit()
end
if not stats then
	print("SlmodStats.lua didn\'t contain a stats table")
	os.exit()
end

local stats_length = tableLength(stats)
print("Found " .. stats_length .. " players in stats table:")

-- print players names
print(getPlayers(stats))


-- convert stats table to json
local stats_json = json.encode(stats)

-- write json to disk
writeFile(stats_json, "SlmodStats.json")
print("\nExported compact JSON string to SlmodStats.json")
